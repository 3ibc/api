import {config} from "dotenv";
config(); // chargement des vars d'env

import {MongooseUtils} from "./utils";
import {Mongoose} from "mongoose";
import express from "express";
import cors from "cors";
import {ModelRegistry} from "./models";
import {AuthController} from "./controllers";
import {AuthService} from "./services";
import {TeamController} from "./controllers/team.controller";
import {TeamService} from "./services/team.service";
import {MatchController} from "./controllers/match.controller";
import {MatchService} from "./services/match.service";

async function launchAPI() {
    const db: Mongoose = await MongooseUtils.open();
    const registry = new ModelRegistry(db);

    const app = express();
    app.use(cors());

    const authService = new AuthService(registry);
    const teamService = new TeamService(registry);
    const matchService = new MatchService(registry);
    const authController = new AuthController(authService);
    app.use('/auth', authController.buildRoutes());

    const teamController = new TeamController(authService, teamService);
    app.use('/teams', teamController.buildRoutes());

    const matchController = new MatchController(authService, teamService, matchService);
    app.use('/matches', matchController.buildRoutes());

    app.listen(process.env.PORT, function() {
        console.log(`Listening on ${process.env.PORT}`);
    });
}

launchAPI().catch(console.error);
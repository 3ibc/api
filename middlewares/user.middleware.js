"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserMiddleware = void 0;
class UserMiddleware {
    static isAdmin() {
        return function (req, res, next) {
            const user = req.user;
            if (!user) {
                res.status(401).end();
                return;
            }
            if (typeof user === 'string') {
                res.status(403).end();
                return;
            }
            const indexOf = user.accesses.indexOf('admin');
            if (indexOf === -1) {
                res.status(403).end();
                return;
            }
            next(); // is admin user
        };
    }
}
exports.UserMiddleware = UserMiddleware;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.matchSchema = exports.matchResultSchema = void 0;
const mongoose_1 = require("mongoose");
exports.matchResultSchema = new mongoose_1.Schema({
    team: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Team'
    },
    score: {
        type: mongoose_1.Schema.Types.Number,
        default: 0
    }
}, {
    _id: false,
    versionKey: false
});
exports.matchSchema = new mongoose_1.Schema({
    home: exports.matchResultSchema,
    away: exports.matchResultSchema,
    date: {
        type: mongoose_1.Schema.Types.Date,
        required: true
    }
}, {
    versionKey: false // permet d'enlever le __v des documents
});

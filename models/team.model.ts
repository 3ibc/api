import {Schema} from "mongoose";
import {SafeDeletionModel} from "./safe-deletion.model";

export interface ITeam {
    name: string;
    city: string;
    country: string;
    members: number;
}

export const teamSchema = new Schema<ITeam & SafeDeletionModel>({
    name: {
        type: Schema.Types.String,
        index: true, // si recherche a faire il faut mettre un index
        required: true,
        unique: true
    },
    city: {
        type: Schema.Types.String,
        required: true
    },
    country: {
        type: Schema.Types.String,
        required: true
    },
    members: {
        type: Schema.Types.Number,
        required: true
    },
    deletedAt: {
        type: Schema.Types.Date,
        default: null
    }
}, {
    versionKey: false, // permet d'enlever le __v des documents
});
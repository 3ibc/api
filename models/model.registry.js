"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModelRegistry = void 0;
const user_model_1 = require("./user.model");
const session_model_1 = require("./session.model");
const team_model_1 = require("./team.model");
const match_model_1 = require("./match.model");
class ModelRegistry {
    constructor(mongoose) {
        this.mongoose = mongoose;
        this.userModel = mongoose.model('User', user_model_1.userSchema);
        this.sessionModel = mongoose.model('Session', session_model_1.sessionSchema);
        this.teamModel = mongoose.model('Team', team_model_1.teamSchema);
        this.matchModel = mongoose.model('Match', match_model_1.matchSchema);
    }
}
exports.ModelRegistry = ModelRegistry;

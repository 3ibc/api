import {Schema} from "mongoose";
import {ITeam} from "./team.model";

export interface IMatchResult {
    team: string | ITeam;
    score: number | undefined;
}

export interface IMatch {
    home: IMatchResult;
    away: IMatchResult;
    date: Date;
}

export const matchResultSchema = new Schema<IMatchResult>({
   team: {
       type: Schema.Types.ObjectId,
       ref: 'Team'
   },
    score: {
       type: Schema.Types.Number,
       default: 0
    }
}, {
    _id: false,
    versionKey: false
});

export const matchSchema = new Schema<IMatch>({
    home: matchResultSchema,
    away: matchResultSchema,
    date: {
        type: Schema.Types.Date,
        required: true
    }
}, {
    versionKey: false // permet d'enlever le __v des documents
});
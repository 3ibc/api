"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.teamSchema = void 0;
const mongoose_1 = require("mongoose");
exports.teamSchema = new mongoose_1.Schema({
    name: {
        type: mongoose_1.Schema.Types.String,
        index: true, // si recherche a faire il faut mettre un index
        required: true,
        unique: true
    },
    city: {
        type: mongoose_1.Schema.Types.String,
        required: true
    },
    country: {
        type: mongoose_1.Schema.Types.String,
        required: true
    },
    members: {
        type: mongoose_1.Schema.Types.Number,
        required: true
    },
    deletedAt: {
        type: mongoose_1.Schema.Types.Date,
        default: null
    }
}, {
    versionKey: false, // permet d'enlever le __v des documents
});

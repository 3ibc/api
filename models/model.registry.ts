import {Model, Mongoose} from "mongoose";
import {IUser, userSchema} from "./user.model";
import {ISession, sessionSchema} from "./session.model";
import {ITeam, teamSchema} from "./team.model";
import {IMatch, matchSchema} from "./match.model";

export class ModelRegistry {
    readonly mongoose: Mongoose;
    readonly userModel: Model<IUser>;
    readonly sessionModel: Model<ISession>;
    readonly teamModel: Model<ITeam>;
    readonly matchModel: Model<IMatch>;

    constructor(mongoose: Mongoose) {
        this.mongoose = mongoose;
        this.userModel = mongoose.model('User', userSchema);
        this.sessionModel = mongoose.model('Session', sessionSchema);
        this.teamModel = mongoose.model('Team', teamSchema);
        this.matchModel = mongoose.model('Match', matchSchema);
    }
}
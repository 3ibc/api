"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userSchema = void 0;
const mongoose_1 = require("mongoose");
exports.userSchema = new mongoose_1.Schema({
    login: {
        type: mongoose_1.Schema.Types.String,
        index: true, // si recherche a faire il faut mettre un index
        required: true,
        unique: true
    },
    password: {
        type: mongoose_1.Schema.Types.String,
        required: true
    },
    accesses: {
        type: [mongoose_1.Schema.Types.String],
        required: true
    }
}, {
    versionKey: false // permet d'enlever le __v des documents
});

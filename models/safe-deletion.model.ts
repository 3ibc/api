export interface SafeDeletionModel {
    deletedAt?: Date;
}
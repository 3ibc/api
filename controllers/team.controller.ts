import express from "express";
import {Router, Request, Response} from "express";
import {AuthService} from "../services";
import {ServiceErrorCode} from "../services/service.result";
import {SessionMiddleware} from "../middlewares/session.middleware";
import {UserMiddleware} from "../middlewares/user.middleware";
import {TeamService} from "../services/team.service";

export class TeamController {

    constructor(private authService: AuthService,
                private teamService: TeamService) {
    }

    async createTeam(req: Request, res: Response) {
        const sr = await this.teamService.create(
            req.body.name,
            req.body.city,
            req.body.country,
            req.body.members
        );
        switch (sr.errorCode) {
            case ServiceErrorCode.success:
                res.status(201).json(sr.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async getAllTeams(req: Request, res: Response) {
        const sr = await this.teamService.getAll();
        switch (sr.errorCode) {
            case ServiceErrorCode.success:
                res.json(sr.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async deleteTeam(req: Request, res: Response) {
        const teamId = req.params['team_id'];
        const sr = await this.teamService.deleteById(teamId);
        switch (sr.errorCode) {
            case ServiceErrorCode.success:
                res.json(sr.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async updateTeam(req: Request, res: Response) {
        const teamId = req.params.id;
        const teamResult = await this.teamService.updateTeam({
            id: teamId,
            name: req.body.name,
            country: req.body.country,
            members: req.body.members
        });
        switch (teamResult.errorCode) {
            case ServiceErrorCode.success:
                res.status(200).json(teamResult.result);
                break;
            case ServiceErrorCode.notFound:
                res.status(404).end();
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async getById(req: Request, res: Response) {
        const id = req.params['team_id'];
        const sr = await this.teamService.getById(id);
        switch (sr.errorCode) {
            case ServiceErrorCode.success:
                res.json(sr.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    buildRoutes(): Router {
        const router = express.Router();
        router.post('/',
            express.json(),
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.createTeam.bind(this));
        router.patch('/:id',
            express.json(),
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.updateTeam.bind(this));
        router.delete('/:team_id',
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.deleteTeam.bind(this));
        router.get('/',
            SessionMiddleware.isLogged(this.authService),
            this.getAllTeams.bind(this));
        router.get('/:team_id',
            SessionMiddleware.isLogged(this.authService),
            this.getById.bind(this));
        return router;
    }
}
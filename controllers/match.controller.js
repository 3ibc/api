"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchController = void 0;
const express_1 = __importDefault(require("express"));
const session_middleware_1 = require("../middlewares/session.middleware");
const user_middleware_1 = require("../middlewares/user.middleware");
const service_result_1 = require("../services/service.result");
class MatchController {
    constructor(authService, teamService, matchService) {
        this.authService = authService;
        this.teamService = teamService;
        this.matchService = matchService;
    }
    createMatch(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            if (req.body.home === req.body.away) {
                res.status(400).end();
                return;
            }
            const homeResult = yield this.teamService.getById(req.body.home);
            if (homeResult.errorCode !== service_result_1.ServiceErrorCode.success) {
                res.status(400).end();
                return;
            }
            const awayResult = yield this.teamService.getById(req.body.away);
            if (awayResult.errorCode !== service_result_1.ServiceErrorCode.success) {
                res.status(400).end();
                return;
            }
            const sr = yield this.matchService.create(req.body.date, homeResult.result, // ! permet d'enlever la possibilité à la variable d'etre optionnelle
            awayResult.result);
            switch (sr.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.status(201).json(sr.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    updateMatch(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const matchId = req.params.id;
            const matchResult = yield this.matchService.updateMatch({
                id: matchId,
                date: req.body.date,
                homeScore: req.body.home_score,
                awayScore: req.body.away_score,
            });
            switch (matchResult.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.status(200).json(matchResult.result);
                    break;
                case service_result_1.ServiceErrorCode.notFound:
                    res.status(404).end();
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    deleteMatch(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const matchId = req.params.id;
            const matchResult = yield this.matchService.deleteMatch(matchId);
            switch (matchResult.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.status(204).end();
                    break;
                case service_result_1.ServiceErrorCode.notFound:
                    res.status(404).end();
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    searchMatch(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const teamId = req.query.team_id; // -> ?team_id=XXX
            const matchResult = yield this.matchService.searchMatches(teamId);
            switch (matchResult.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.json(matchResult.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    buildRoutes() {
        const router = express_1.default.Router();
        router.get('/', session_middleware_1.SessionMiddleware.isLogged(this.authService), this.searchMatch.bind(this));
        router.post('/', express_1.default.json(), session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.createMatch.bind(this));
        router.patch('/:id', express_1.default.json(), session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.updateMatch.bind(this));
        router.delete('/:id', session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.deleteMatch.bind(this));
        return router;
    }
}
exports.MatchController = MatchController;

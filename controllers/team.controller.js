"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeamController = void 0;
const express_1 = __importDefault(require("express"));
const service_result_1 = require("../services/service.result");
const session_middleware_1 = require("../middlewares/session.middleware");
const user_middleware_1 = require("../middlewares/user.middleware");
class TeamController {
    constructor(authService, teamService) {
        this.authService = authService;
        this.teamService = teamService;
    }
    createTeam(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const sr = yield this.teamService.create(req.body.name, req.body.city, req.body.country, req.body.members);
            switch (sr.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.status(201).json(sr.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    getAllTeams(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const sr = yield this.teamService.getAll();
            switch (sr.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.json(sr.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    deleteTeam(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const teamId = req.params['team_id'];
            const sr = yield this.teamService.deleteById(teamId);
            switch (sr.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.json(sr.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    updateTeam(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const teamId = req.params.id;
            const teamResult = yield this.teamService.updateTeam({
                id: teamId,
                name: req.body.name,
                country: req.body.country,
                members: req.body.members
            });
            switch (teamResult.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.status(200).json(teamResult.result);
                    break;
                case service_result_1.ServiceErrorCode.notFound:
                    res.status(404).end();
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    getById(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const id = req.params['team_id'];
            const sr = yield this.teamService.getById(id);
            switch (sr.errorCode) {
                case service_result_1.ServiceErrorCode.success:
                    res.json(sr.result);
                    break;
                default:
                    res.status(500).end();
                    break;
            }
        });
    }
    buildRoutes() {
        const router = express_1.default.Router();
        router.post('/', express_1.default.json(), session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.createTeam.bind(this));
        router.patch('/:id', express_1.default.json(), session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.updateTeam.bind(this));
        router.delete('/:team_id', session_middleware_1.SessionMiddleware.isLogged(this.authService), user_middleware_1.UserMiddleware.isAdmin(), this.deleteTeam.bind(this));
        router.get('/', session_middleware_1.SessionMiddleware.isLogged(this.authService), this.getAllTeams.bind(this));
        router.get('/:team_id', session_middleware_1.SessionMiddleware.isLogged(this.authService), this.getById.bind(this));
        return router;
    }
}
exports.TeamController = TeamController;

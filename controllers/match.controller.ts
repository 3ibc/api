import express, {Request, Response, Router} from "express";
import {AuthService} from "../services";
import {SessionMiddleware} from "../middlewares/session.middleware";
import {UserMiddleware} from "../middlewares/user.middleware";
import {MatchService} from "../services/match.service";
import {ServiceErrorCode} from "../services/service.result";
import {TeamService} from "../services/team.service";

export class MatchController {

    constructor(private authService: AuthService,
                private teamService: TeamService,
                private matchService: MatchService) {
    }

    async createMatch(req: Request, res: Response) {
        if(req.body.home === req.body.away) {
            res.status(400).end();
            return;
        }
        const homeResult = await this.teamService.getById(req.body.home);
        if(homeResult.errorCode !== ServiceErrorCode.success) {
            res.status(400).end();
            return;
        }
        const awayResult = await this.teamService.getById(req.body.away);
        if(awayResult.errorCode !== ServiceErrorCode.success) {
            res.status(400).end();
            return;
        }
        const sr = await this.matchService.create(
            req.body.date,
            homeResult.result!, // ! permet d'enlever la possibilité à la variable d'etre optionnelle
            awayResult.result!
        );
        switch (sr.errorCode) {
            case ServiceErrorCode.success:
                res.status(201).json(sr.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async updateMatch(req: Request, res: Response) {
        const matchId = req.params.id;
        const matchResult = await this.matchService.updateMatch({
            id: matchId,
            date: req.body.date,
            homeScore: req.body.home_score,
            awayScore: req.body.away_score,
        });
        switch (matchResult.errorCode) {
            case ServiceErrorCode.success:
                res.status(200).json(matchResult.result);
                break;
            case ServiceErrorCode.notFound:
                res.status(404).end();
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async deleteMatch(req: Request, res: Response) {
        const matchId = req.params.id;
        const matchResult = await this.matchService.deleteMatch(matchId);
        switch (matchResult.errorCode) {
            case ServiceErrorCode.success:
                res.status(204).end();
                break;
            case ServiceErrorCode.notFound:
                res.status(404).end();
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    async searchMatch(req: Request, res: Response) {
        const teamId = req.query.team_id; // -> ?team_id=XXX
        const matchResult = await this.matchService.searchMatches(teamId as string);
        switch (matchResult.errorCode) {
            case ServiceErrorCode.success:
                res.json(matchResult.result);
                break;
            default:
                res.status(500).end();
                break;
        }
    }

    buildRoutes(): Router {
        const router = express.Router();
        router.get('/',
            SessionMiddleware.isLogged(this.authService),
            this.searchMatch.bind(this));
        router.post('/',
            express.json(),
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.createMatch.bind(this));
        router.patch('/:id',
            express.json(),
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.updateMatch.bind(this));
        router.delete('/:id',
            SessionMiddleware.isLogged(this.authService),
            UserMiddleware.isAdmin(),
            this.deleteMatch.bind(this));
        return router;
    }
}
import {FilterQuery, Model, Types} from "mongoose";
import {ITeam, ModelRegistry} from "../models";
import {ServiceResult} from "./service.result";
import {IMatch} from "../models/match.model";

export interface MatchUpdates {
    id: string; // id du match
    date?: Date;
    homeScore?: number;
    awayScore?: number;
}

export class MatchService {

    private matchModel: Model<IMatch>;

    constructor(private modelRegistry: ModelRegistry) {
        this.matchModel = modelRegistry.matchModel;
    }

    async create(date: Date, home: ITeam, away: ITeam): Promise<ServiceResult<IMatch>> {
        try {
            const match = await this.matchModel.create({
                date: date,
                home: {
                    team: home,
                },
                away: {
                    team: away
                }
            });
            return ServiceResult.success(match);
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async updateMatch(updates: MatchUpdates): Promise<ServiceResult<IMatch>> {
        const matchUpdates: {[key: string]: any} = {};
        if(updates.date !== undefined) {
            matchUpdates.date = updates.date;
        }
        if(updates.homeScore !== undefined) {
            matchUpdates["home.score"] = updates.homeScore;
        }
        if(updates.awayScore !== undefined) {
            matchUpdates["away.score"] = updates.awayScore;
        }
        try {
            const match = await this.matchModel.findByIdAndUpdate(updates.id, {
                $set: matchUpdates
            }, {new: true}).exec();
            if(match) {
                return ServiceResult.success(match);
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async getById(id: string): Promise<ServiceResult<IMatch>> {
        try {
            const match = await this.matchModel.findById(id).exec();
            if(match) {
                return ServiceResult.success(match);
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async deleteMatch(matchId: string): Promise<ServiceResult<IMatch>> {
        try {
            const match = await this.matchModel.findByIdAndDelete(matchId).exec();
            if(match) {
                return ServiceResult.success(match);
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async searchMatches(teamId?: string): Promise<ServiceResult<IMatch[]>> {
        try {
            const query: FilterQuery<IMatch> = {};
            if(teamId && Types.ObjectId.isValid(teamId)) {
                query['$or'] = [
                    {"home.team": teamId},
                    {"away.team": teamId}
                ];
            }
            const matches = await this.matchModel.find(query)
                                                            .populate('home.team') // permet de charger les models derriere l'id
                                                            .populate('away.team')
                                                            .exec();
            return ServiceResult.success(matches);
        } catch(err) {
            return ServiceResult.failed();
        }
    }
}
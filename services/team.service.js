"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TeamService = void 0;
const mongoose_1 = require("mongoose");
const service_result_1 = require("./service.result");
class TeamService {
    constructor(modelRegistry) {
        this.modelRegistry = modelRegistry;
        this.teamModel = modelRegistry.teamModel;
    }
    create(name, city, country, members) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const team = yield this.teamModel.create({
                    name: name,
                    city: city,
                    country: country,
                    members: members
                });
                return service_result_1.ServiceResult.success(team);
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const teams = yield this.teamModel.find({
                    deletedAt: null
                }).exec();
                return service_result_1.ServiceResult.success(teams);
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    getById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const team = yield this.teamModel.findById(id).exec();
                if (team) {
                    return service_result_1.ServiceResult.success(team);
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    deleteById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (mongoose_1.Types.ObjectId.isValid(id)) {
                    const team = yield this.teamModel.findByIdAndUpdate(id, {
                        $set: {
                            deletedAt: new Date()
                        }
                    }, {
                        new: true // permet de récuperer l'element updated
                    }).exec();
                    if (team) {
                        return service_result_1.ServiceResult.success(team);
                    }
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    updateTeam(updates) {
        return __awaiter(this, void 0, void 0, function* () {
            const teamUpdates = {};
            if (updates.members !== undefined) {
                teamUpdates.members = updates.members;
            }
            if (updates.name !== undefined) {
                teamUpdates["name"] = updates.name;
            }
            if (updates.country !== undefined) {
                teamUpdates["country"] = updates.country;
            }
            try {
                const team = yield this.teamModel.findByIdAndUpdate(updates.id, {
                    $set: teamUpdates,
                }, { new: true }).exec();
                if (team) {
                    return service_result_1.ServiceResult.success(team);
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
}
exports.TeamService = TeamService;

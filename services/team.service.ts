import {Model, Types} from "mongoose";
import {ITeam, ModelRegistry} from "../models";
import {ServiceResult} from "./service.result";
import {IMatch} from "../models/match.model";
import {MatchUpdates} from "./match.service";

// Partial permet de rendre toutes les clés du type générique en parametre optionnel
// Pick permet de selectionner des clés d'un type
export type TeamUpdates = Partial<Pick<ITeam, "country" | "name" | "members">> & {id: string}

export class TeamService {

    private teamModel: Model<ITeam>;

    constructor(private modelRegistry: ModelRegistry) {
        this.teamModel = modelRegistry.teamModel;
    }

    async create(name: string, city: string, country: string, members: number): Promise<ServiceResult<ITeam>> {
        try {
            const team = await this.teamModel.create({
                name: name,
                city: city,
                country: country,
                members: members
            });
            return ServiceResult.success(team);
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async getAll(): Promise<ServiceResult<ITeam[]>> {
        try {
            const teams = await this.teamModel.find({
                deletedAt: null
            }).exec();
            return ServiceResult.success(teams);
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async getById(id: string): Promise<ServiceResult<ITeam>> {
        try {
            const team = await this.teamModel.findById(id).exec();
            if(team) {
                return ServiceResult.success(team);
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async deleteById(id: string): Promise<ServiceResult<ITeam>> {
        try {
            if(Types.ObjectId.isValid(id)) {
                const team = await this.teamModel.findByIdAndUpdate(id, {
                    $set: {
                        deletedAt: new Date()
                    }
                }, {
                    new: true // permet de récuperer l'element updated
                }).exec();
                if(team) {
                    return ServiceResult.success(team);
                }
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    async updateTeam(updates: TeamUpdates): Promise<ServiceResult<ITeam>> {
        const teamUpdates: {[key: string]: any} = {};
        if(updates.members !== undefined) {
            teamUpdates.members = updates.members;
        }
        if(updates.name !== undefined) {
            teamUpdates["name"] = updates.name;
        }
        if(updates.country !== undefined) {
            teamUpdates["country"] = updates.country;
        }
        try {
            const team = await this.teamModel.findByIdAndUpdate(updates.id, {
                $set: teamUpdates,
            }, {new: true}).exec();
            if(team) {
                return ServiceResult.success(team);
            }
            return ServiceResult.notFound();
        } catch(err) {
            return ServiceResult.failed();
        }
    }
}
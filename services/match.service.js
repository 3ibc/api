"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchService = void 0;
const mongoose_1 = require("mongoose");
const service_result_1 = require("./service.result");
class MatchService {
    constructor(modelRegistry) {
        this.modelRegistry = modelRegistry;
        this.matchModel = modelRegistry.matchModel;
    }
    create(date, home, away) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const match = yield this.matchModel.create({
                    date: date,
                    home: {
                        team: home,
                    },
                    away: {
                        team: away
                    }
                });
                return service_result_1.ServiceResult.success(match);
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    updateMatch(updates) {
        return __awaiter(this, void 0, void 0, function* () {
            const matchUpdates = {};
            if (updates.date !== undefined) {
                matchUpdates.date = updates.date;
            }
            if (updates.homeScore !== undefined) {
                matchUpdates["home.score"] = updates.homeScore;
            }
            if (updates.awayScore !== undefined) {
                matchUpdates["away.score"] = updates.awayScore;
            }
            try {
                const match = yield this.matchModel.findByIdAndUpdate(updates.id, {
                    $set: matchUpdates
                }, { new: true }).exec();
                if (match) {
                    return service_result_1.ServiceResult.success(match);
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    getById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const match = yield this.matchModel.findById(id).exec();
                if (match) {
                    return service_result_1.ServiceResult.success(match);
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    deleteMatch(matchId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const match = yield this.matchModel.findByIdAndDelete(matchId).exec();
                if (match) {
                    return service_result_1.ServiceResult.success(match);
                }
                return service_result_1.ServiceResult.notFound();
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
    searchMatches(teamId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = {};
                if (teamId && mongoose_1.Types.ObjectId.isValid(teamId)) {
                    query['$or'] = [
                        { "home.team": teamId },
                        { "away.team": teamId }
                    ];
                }
                const matches = yield this.matchModel.find(query)
                    .populate('home.team') // permet de charger les models derriere l'id
                    .populate('away.team')
                    .exec();
                return service_result_1.ServiceResult.success(matches);
            }
            catch (err) {
                return service_result_1.ServiceResult.failed();
            }
        });
    }
}
exports.MatchService = MatchService;
